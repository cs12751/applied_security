Stage 1:

Q.1.1 Consider an example where N = 551, e = 5, d = 101 and c = 243.  Using
      your own words, briefly explain and illustrate the working principle
      of this attack (i.e., how and why it works).
>>>>>>>>
The principle of this attack is that an atacker has access to the public RSA key and is able to adaptively send ciphertexts to the server under attack. Based on the error messages, the attacker is able to narrow down the message such that he can use a binary search to recover the message m.
>>>>>>>>
Q.1.2 To prevent the attack, the vendor suggests altering the software: the
      idea is that no matter what error occurs, the same error code will be
      produced.  Explain whether (and why, or why not) this change alone is
      sufficient.

>>>>>>>>
The same idea of this question is explained in the document at ftp://ftp.rsasecurity.com/pub/pkcs/pkcs-1/pkcs-1v2-1d3, so we try to emphasize the fact that, if we change the software, we will not be able to distinguish between error messages, therefore we could not attack the device based on error messages, you could exploit the fact that you can time the differences between when you get back the messages and use statistics on that data.
>>>>>>>>

Q.1.6 RSA has a homomorphic property: explain what this means, and how it
      relates to this attack.

>>>>>>>>
If the RSA public key is modulus N and exponent e, then the encryption of a message m is given by m^e mod N. The homomorphic property is then:
-----> c1 = (x1 ^ e) mod N 
-----> c2 = (x2 ^ e) mod N
=====> c3 = c1 * c2 = (x1 ^ e) * (x2 ^ e) mod N

This also means that the Encryption is maleable.
>>>>>>>>

Stage 2:

Q.2.1 The vendor of this attack target is concerned that news of this attack
      could scare off potential customers; they will make any alteration
      necessary (in software or hardware) to prevent the attack.  Explain
      the options they have, and which one you would recommend.

>>>>>>>>
We know that this attack is based on the Montgomery Reduction which happens when the intermediate result of the multiplication exceeds the RSA modulus N, in which case we need to perform the reduction, thus this extra step allows the atacker to perform statistical analysis to guess bits of the key.

There are a number of defense methods available to counter this atack, such as:
---> RSA blinding, which basically means we introduce randomness into the RSA computations to make timing information unusable.
---> Execute the Montgomery Reduction even though  we don't need it, discard the result, therefore masking the time leaks and preventing analysis.

Both options are viable for implementation, since their common drawback is the fact that they take a bit more time to run.
>>>>>>>>

Q.2.2 Assuming a Montgomery multiplication takes n (simulated) clock cycles
      on the attack target, estimate the value of n (as accurately as you
      can) and explain how you did so.
>>>>>>>>

>>>>>>>>

Q.2.7 Numerous factors might produce noise within measurements of execution
      time: based on the given attack target and context, outline at least
      two examples.
>>>>>>>>
This attack can also be treated as a signal detection problem. Basically, the signal is represented by the time variations due to the target exponent bit, while the noise results from measurement inaccuracies timing variations due to unknown exponent bits. Measurement inaccuracies could also arise from performing the attack on a network.
>>>>>>>>

Stage 3:

Q.3.1 Imagine that instead of influencing data (i.e., the state matrix), a
      fault can influence control-flow (e.g., conditional statements, loop
      structure) somehow during execution.  Stating any assumptions you
      make, give an alternative attack strategy based on this ability.
>>>>>>>>
In the assumption that we've got access to the power supply of the target device, we can use a high-precise power spike to influence the control flow of the program. In the case of AES we could tamper with the program counter/ loop bound such that we don't execute the Rounds loop, if it's possible -- or at least the number of loop iterations is small enough --, making it easy to brute force the remaining operations and perform a successful attack. For the suggestions on this question, I followed information from the paper: 

https://www.cosic.esat.kuleuven.be/publications/article-2204.pdf
>>>>>>>>

Q.3.2 The question does not specify how faults are induced.  For the given
      attack target and context, explain one viable approach.
>>>>>>>>

Fault attacks are usually induced by influencing the hardware using power spikes, clock glitches, temperature attacks, optical attacks or Electro-Magnetic attacks. For example, one can reduce the power supply (Voltage) to the processor of the microprocessor, and also keep the clock-frequency at the same value, basically inducing a fault into the device, since the two are dependant.

>>>>>>>>
Q.3.3 For side-channel attacks, the number of acquisitions required can be
      used as a measure of efficiency.  Outline the equivalent measure for
      fault attacks, and explain whether (and why) you think a side-channel
      attack on AES could be as efficient as a fault attack based on these
      measures.
>>>>>>>>

In order to perform an efficient fault attack, that means that the fault should not alter the device in such a way that it's not going to be used, since it's going to malfunction. Therefore, a good measure would be to use less faults with better results. 

>>>>>>>>

Stage 4:

Q.4.1 As a security consultant, you have been working on a prototype of the
      attack target; before the product is deployed, the vendor is willing
      to make any alteration necessary (in software or hardware) to prevent
      the attack.  Explain the options available, and which one you would
      recommend.
>>>>>>>>
-->First approach will be to reduce the signal sizes, such as choosing operations that leak less information in the power consumption, balancing Hamming Weights and also by physically protecting the device. But this approaches will not reduce signal size to zero, therefore an attacker with high-computational power would be able to atack the device.
-->Second approach would be to introduce noise into the power consumption data, which would increase the number of trace samples which an attacker would require to get in order to attack.
-->Final approach involves designing crypto with realistic knowledge about the hardware it's going to be used. One example of a valid defense would be to have a key counter in order to prevent an attacker from gathering large number of samples. Alternatively, using Hash functions would disable partial information about the key.

As a recommendation, the third option would be a viable one.

>>>>>>>>
Q.4.2 Your implementation of this attack will target some operation during
      execution of AES.  Identify which operation you target, and explain
      why.  Outline other operation(s) you could have targeted, and why they
      might be a better or worse choice.

We target the SubBytes in AES, since it consumes a fair amount of power. Because your receive a valid ciphertext from the device, you can apply a similar attack to recover the 10th round SubKey used in the last AddRoundKey operation. This attack would be a bit less efficient as you would have to perform an extra operation to reverse the KeyExpansion operation.


Q.4.3 Suppose the attack target executes AES in CBC mode rather than ECB.
      How would you alter your attack strategy to cope with this change?

Since the CBC mode uses the random IV at the begginign of the encryption algorithm, we can use the IV to xor it with the message and use the result to continue the same steps of the ECB attack.