import sys, subprocess, hashlib
import string
import math
import os
import threading
#import Crypto.Util.number
global b
b = 2**64
global timings #stores mock-ciphers timings
timings = {}
global omega 

global bucket1 
bucket1 = []
global bucket2 
bucket2 = []
global bucket3 
bucket3 = []
global bucket4 
bucket4 = []

# global mock_ciphers
# mock_ciphers = []


##########################################
#######		MONTGOMERY				######
##########################################

def to_binary(x) :
	return bin(x)

def get_ith_limb(x, i) :
	bin_mask = 18446744073709551615
	#x = int(x, 16)
	return (x >> 64 * i) & bin_mask

def get_limb_number(N) :
	return math.ceil(math.log( N, 2**64 ))

def Mont_Rho_Squared(b, N) :
	t = 1
	w = 64
	#print 2*get_limb_size(N)*w
	for i in range (0, int(2*get_limb_number(N)*w)) :
		t = (t + t) % N
	return t

def Mont_Omega(b, N) :
	t = 1
	w = 64
	for i in range(0, w - 1) :
		t *= t * N % b
	return -t % b

def Mont_Mul(b, x, y, N, omega) :
	flag = False
	r = 0
	x0 = get_ith_limb(x, 0)
	#omega = Mont_Omega(b, N)
	#print omega
	for i in range(0, int(get_limb_number(N))) :
		r0 = get_ith_limb(r, 0)
		yi = get_ith_limb(y, i)
		u = ((r0 + yi * x0) * omega) % b
		r = (r + yi * x + u * N)/b
	if (r >= N) :
		flag = True
		r -= N
	return r, flag

def Mont_Exp (b, x, y, N, omega) :
	rho_sq = Mont_Rho_Squared(b, N)
	t_hat = Mont_Mul(b, 1, rho_sq, N, omega)[0]
	x_hat = Mont_Mul(b, x, rho_sq, N, omega)[0]
	y_bin = y

	for i in range(0, len(y_bin)) :
		
		t_hat = Mont_Mul(b, t_hat, t_hat, N, omega)[0]

		if (y_bin[i] == "1") :
			t_hat = Mont_Mul(b, t_hat, x_hat, N, omega)[0]
	
	t_hat = Mont_Mul(b, t_hat, t_hat, N, omega)[0]

	t_temp = t_hat
	#bit0
	t_hat, flag = Mont_Mul(b, t_temp, t_temp, N, omega)

	#bit1
	t_hat = Mont_Mul(b, t_temp, x_hat, N, omega)[0]
	t_hat, flag1 = Mont_Mul(b, t_hat, t_hat, N, omega)

	return flag, flag1
	#return Mont_Mul(b, t_hat, 1, N)#, flag

#########################################
#########		END MONT 	 ############
#########################################
def gen_Random_cipher() :
	#print os.urandom(8).encode('hex')
	#return int (os.urandom(8).encode('hex'), 16)# + os.urandom(16).encode('hex') + os.urandom(16).encode('hex') + os.urandom(16).encode('hex'),16)
	return int(os.urandom(64).encode('hex'),16)

def gen_ciphers_list(k) :
	list = []
	for i in range(0, k) :
		list.append(gen_Random_cipher())
	return list

def add_timing(c, t) :
	timings[c] = t

def get_avg(list) :
	sum = 0
	for value in list :
		sum += int(value)
	#print len(list)
	return (sum / (1.0*len(list)))	

def readFile(src) :
  file = open(src, "r")
  array = []
  for line in file:
    array.append(line)
  file.close
  return ( int(array[0],16), int(array[1],16)) #return N, e

def interact( c ) :
  #print hex(c).rstrip("L").lstrip("0x")
  c = hex(c).rstrip("L").lstrip("0x").zfill(256)
  target_in.write( "%s\n" % ( c ) ) ; target_in.flush()

  # Receive error code from attack target.
  t = ( target_out.readline().strip() )
  m = ( target_out.readline().strip() )
  return ( t, m )

def interactR( c, N, d ) :
  #print hex(c).rstrip("L").lstrip("0x")
  c_string = hex(c).rstrip("L").lstrip("0x")
  N_string =hex(N).rstrip("L").lstrip("0x")

  c_string = c_string.zfill(256)
  N_string = N_string.zfill(256)
  d = d.zfill(256)

  target_inR.write( "%s\n%s\n%s\n" % (c_string,N_string,d ) ) ; target_inR.flush()

  # Receive error code from attack target.
  t = ( target_outR.readline().strip() )
  m = ( target_outR.readline().strip() )
  return ( t, m )



def montCipher(d, N, omega, tid) :
	#print "hello"
	#print len(mock_ciphers)
	for i in range(tid*len(mock_ciphers)/2, ((tid+1)*len(mock_ciphers)/2)) :
		#print len
		flagfor0, flagfor1 = Mont_Exp(2**64, mock_ciphers[i], d, N, omega)
		#print (timings[mock_ciphers[i]])
		if flagfor1:
		 	bucket1.append(timings[ mock_ciphers[i] ])
		   	#print bucket1[timings[mock_ciphers[i]]]
		else:
		    bucket2.append(timings[ mock_ciphers[i] ])
		    #print bucket2[timings[mock_ciphers[i]]]
		if flagfor0:
		   	bucket3.append(timings[ mock_ciphers[i] ])
		   	#print bucket3[timings[mock_ciphers[i]]]
		else:
		   	bucket4.append(timings[ mock_ciphers[i] ])
		   	#print bucket4[timings[mock_ciphers[i]]]

def checkKey(d, i) :
	#print "Found guess key"
	#gen random c
	rand_c = gen_Random_cipher()
	#send c/ get m' from .D
	t, m = interact (rand_c)
	#check for last bit
	d_1 = d[:i] + "0"
	d_1 = hex(int(d_1,2)).rstrip("L").lstrip("0x")
	d_2 = d[:i] + "1"
	d_2 = hex(int(d_2,2)).rstrip("L").lstrip("0x")
	# #interact with .R
	t, m_p1 = interactR (rand_c, N, d_1)
	#print m_p1
	t, m_p2 = interactR (rand_c, N, d_2)
	#print m_p2

	if (m == m_p1) :
		print ("Key is ", d_1)
	if (m == m_p2) :
		print ("Key is ", d_2)

def attack (N) :
	d = "1" #private key guess
	omega = Mont_Omega(b, N)
	global mock_ciphers 
	mock_ciphers = gen_ciphers_list(10000)
	#print len(mock_ciphers)
	for c in mock_ciphers :
	   	( t, m ) = interact(c)
	   	add_timing( c, t)
	#print len(timings)
	#print timings
	print "Started loop: " 
	for i in range(0, 64) :
   	#print ("Iteration",i)
   		print ("key", d)
   	#print("Init buckets")
 	# bucket1 = []
	# bucket2 = []
	# bucket3 = []
	# bucket4 = []
   	#for i in range(0, len(mock_ciphers)/8) :
		t1 = threading.Thread(target=montCipher,args=(d,N,omega,0))
		t1.start()
		t2 = threading.Thread(target=montCipher,args=(d,N,omega,1))
		t2.start()

		t1.join()
		t2.join()

		   		#print c
	   		# flagfor0, flagfor1 = Mont_Exp(2**64, c, d, N, omega)
	   		# #print flagfor1, flagfor0
	   		# if flagfor1:
	   		# 	bucket1.append(timings[c])
	   		# 	#print timings[c]
	   		# else:
	   		# 	bucket2.append(timings[c])
	   		# if flagfor0:
	   		# 	bucket3.append(timings[c])
	   		# else:
	   		# 	bucket4.append(timings[c])
   	# print get_avg(bucket1)
   	# print get_avg(bucket2)
   	# print get_avg(bucket3)
   	# print get_avg(bucket4)

		dif1 = abs(get_avg(bucket1) - get_avg(bucket2))
		dif2 = abs(get_avg(bucket3) - get_avg(bucket4))
		print (dif1, dif2)
		del bucket1[:]
		del bucket2[:]
		del bucket3[:]
		del bucket4[:]

	  	if(dif1 > dif2):
	  		d += "1"
	  	else:
	  		d += "0"
	  	print d
	  	checkKey(d, i)
	#d = "10011111111101101110011101100011100001001110100011001001101110"
	

if ( __name__ == "__main__" ) :
  # Produce a sub-process representing the attack target.
  target = subprocess.Popen( args   = sys.argv[ 1 ],
                             stdout = subprocess.PIPE, 
                             stdin  = subprocess.PIPE )

  # Construct handles to attack target standard input and output.
  target_out = target.stdout
  target_in  = target.stdin

  targetR = subprocess.Popen( args   = sys.argv[ 2 ],
                              stdout = subprocess.PIPE, 
                              stdin  = subprocess.PIPE )

  target_outR = targetR.stdout
  target_inR  = targetR.stdin

  (N, e) = readFile(sys.argv[3])
 
  attack(N)

