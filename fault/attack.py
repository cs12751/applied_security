import sys, subprocess, hashlib
import string
import math
import os
import struct, Crypto.Cipher.AES as AES
from multiprocessing import Pool

# Rijndael S-box
sbox =  [0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67,
        0x2b, 0xfe, 0xd7, 0xab, 0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59,
        0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 0xb7,
        0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1,
        0x71, 0xd8, 0x31, 0x15, 0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05,
        0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83,
        0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29,
        0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b,
        0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 0xd0, 0xef, 0xaa,
        0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c,
        0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc,
        0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec,
        0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19,
        0x73, 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee,
        0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49,
        0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4,
        0xea, 0x65, 0x7a, 0xae, 0x08, 0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6,
        0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 0x70,
        0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9,
        0x86, 0xc1, 0x1d, 0x9e, 0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e,
        0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 0x8c, 0xa1,
        0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0,
        0x54, 0xbb, 0x16]


# Rijndael Inverted S-box
rsbox = [0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3,
        0x9e, 0x81, 0xf3, 0xd7, 0xfb , 0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f,
        0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb , 0x54,
        0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b,
        0x42, 0xfa, 0xc3, 0x4e , 0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24,
        0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25 , 0x72, 0xf8,
        0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d,
        0x65, 0xb6, 0x92 , 0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda,
        0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84 , 0x90, 0xd8, 0xab,
        0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3,
        0x45, 0x06 , 0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1,
        0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b , 0x3a, 0x91, 0x11, 0x41,
        0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6,
        0x73 , 0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9,
        0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e , 0x47, 0xf1, 0x1a, 0x71, 0x1d,
        0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b ,
        0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0,
        0xfe, 0x78, 0xcd, 0x5a, 0xf4 , 0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07,
        0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f , 0x60,
        0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f,
        0x93, 0xc9, 0x9c, 0xef , 0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5,
        0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61 , 0x17, 0x2b,
        0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55,
        0x21, 0x0c, 0x7d]

aes_round_constant = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36]

def aes(k, m):
  k = struct.pack( 16 * "B", *k )
  m = struct.pack( 16 * "B", *m )
  c = AES.new( k ).encrypt( m )
  return struct.unpack(16 * "B", c)

def gf_mul(a, b): 
  p = 0 
  for counter in range(8): 
    if b & 1: 
      p ^= a 
    hi_bit_set = a & 0x80 
    a <<= 1 
    # keep a 8 bit 
    a &= 0xFF 
    if hi_bit_set: 
      a ^= 0x1b 
    b >>= 1 
  return p

def gf_inv(a) : 
  t_0 = gf_mul(a, a) # a^2 
  t_1 = gf_mul(t_0, a) # a^3 
  t_0 = gf_mul(t_0, t_0) # a^4 
  t_1 = gf_mul(t_1, t_0) # a^7 
  t_0 = gf_mul(t_0, t_0) # a^8 
  t_0 = gf_mul(t_1, t_0) # a^15 
  t_0 = gf_mul(t_0, t_0) # a^30 
  t_0 = gf_mul(t_0, t_0) # a^60 
  t_1 = gf_mul(t_1, t_0) # a^67 
  t_0 = gf_mul(t_0, t_1) # a^127 
  t_0 = gf_mul(t_0, t_0) # a^254 
  return t_0

def reverse(r, roundNum) :
  k = [0] * 16
 
  k[4]  = r[0] ^ r[4]
  k[5]  = r[1] ^ r[5]
  k[6]  = r[2] ^ r[6]
  k[7]  = r[3] ^ r[7]
 
  k[8]  = r[4] ^ r[8]
  k[9]  = r[5] ^ r[9]
  k[10] = r[6] ^ r[10]
  k[11] = r[7] ^ r[11]
 
  k[12] = r[8] ^ r[12]
  k[13] = r[9] ^ r[13]
  k[14] = r[10]^ r[14]
  k[15] = r[11]^ r[15]
  
  k[0]  = aes_round_constant[roundNum] ^ sbox[k[13]] ^ r[0]
  k[1]  = sbox[k[14]] ^ r[1]
  k[2]  = sbox[k[15]] ^ r[2]
  k[3]  = sbox[k[12]] ^ r[3]
 
  return k
 
 
def keyReverse(k) :
  key = k[:]
  for i in reversed(xrange(0, 10)):
    key = reverse(key, i)
  return key

def gen_Random_cipher() :
  return os.urandom(16).encode('hex')

def interact() :
  r = '8'
  f = '1'
  p = '0'
  i = '0'
  j = '0'
  param = r+','+f+','+p+','+i+','+j
  #m = gen_Random_cipher()
  m = '014E38AB85CD87E5FE1D39D61C73030C' #correct one
  #m = '359A5B18E6132847AD6D16B0FEB45E53'

  target_in.write( "%s\n" % ("") ) ; target_in.flush()
  target_in.write( "%s\n" % ( m ) ) ; target_in.flush()
  c       = ( target_out.readline().strip() )

  target_in.write( "%s\n" % (param) ) ; target_in.flush()
  target_in.write( "%s\n" % ( m ) ) ; target_in.flush()
  c_prime = ( target_out.readline().strip() )

  m2 = []
  X  = []

  for i in range(0,16):
    m2.append(m[2*i:2*i+2])
    X.append(c[2*i:2*i+2])

  return ( c, c_prime, m2, X )

def xor_strings(xs, ys):
  return "".join(chr(ord(x) ^ ord(y)) for x, y in zip(xs, ys))

def fillDelta( activeK, varK1, varK2, varK3, varK4, deltaHash, deltaSet, deltaConstant, cipherIndex, xs, xprimes ) :
  for k in range(0, 256):   
    a = (int(xs[cipherIndex], 16) ^ k)
    b = (int(xprimes[cipherIndex], 16) ^ k)
    result = rsbox[a] ^ rsbox[b]
    delta = gf_mul(result, gf_inv(deltaConstant))
    deltaSet.add(delta)
    if delta not in deltaHash.keys():
      deltaHash[delta] = { varK1 : [], varK2 : [], varK3 : [], varK4 : [] }
    deltaHash[delta][activeK].append(k) 
  return ( deltaHash, deltaSet )


def genHypo(delta, varK1, varK2, varK3, varK4, deltaHash, finalSet, hypoth ) :
  print "Generating hypothesis"
  for k1 in deltaHash[delta][varK1]:
    for k2 in deltaHash[delta][varK2]:
      for k3 in deltaHash[delta][varK3]:
        for k4 in deltaHash[delta][varK4]:
          #print (varK1, varK2, varK3, varK4)
          hypoth.append((k1, k2, k3, k4))
  return hypoth

def solveEquation(c, c_prime, m2, X):
  print "Solving Equations"
  xs = []
  xprimes = []
  #c = '359A5B18E6132847AD6D16B0FEB45E53'
  #c_prime = '71C86574B48DA0D2C874D730715205B0'
  for i in range(0,16):
    xs.append(c[2*i:2*i+2])
    xprimes.append(c_prime[2*i:2*i+2])

  #print "XS"
  #print xs

  #1st Quartet
  set1 = set()
  set2 = set()
  set3 = set()
  set4 = set() 
  delta1 = {}

  ( delta1, set1 ) = fillDelta('k1', 'k1', 'k14', 'k11', 'k8', delta1, set1, 2, 0, xs, xprimes)
  ( delta1, set2 ) = fillDelta('k14', 'k1', 'k14', 'k11', 'k8', delta1, set2, 1, 13, xs, xprimes)
  ( delta1, set3 ) = fillDelta('k11', 'k1', 'k14', 'k11', 'k8', delta1, set3, 1, 10, xs, xprimes)
  ( delta1, set4 ) = fillDelta('k8', 'k1', 'k14', 'k11', 'k8', delta1, set4, 3, 7, xs, xprimes)
  
  finalSet1 = set1 & set2 & set3 & set4
  hypoth1 = []
  for i in finalSet1:
    hypoth1 = genHypo( i,'k1', 'k14', 'k11', 'k8', delta1, finalSet1, hypoth1 )

  setH1 = set(hypoth1)
  #print setH1

  #2nd Quartet
  set5 = set()
  set6 = set()
  set7 = set()
  set8 = set() 
  delta2 = {}

  ( delta2, set5 ) = fillDelta('k5', 'k5', 'k2', 'k15', 'k12', delta2, set5, 1, 4, xs, xprimes)
  ( delta2, set6 ) = fillDelta('k2', 'k5', 'k2', 'k15', 'k12', delta2, set6, 1, 1, xs, xprimes)
  ( delta2, set7 ) = fillDelta('k15', 'k5', 'k2', 'k15', 'k12', delta2, set7, 3, 14, xs, xprimes)
  ( delta2, set8 ) = fillDelta('k12', 'k5', 'k2', 'k15', 'k12', delta2, set8, 2, 11, xs, xprimes)

  finalSet2 = set5 & set6 & set7 & set8
  hypoth2 = []
  for i in finalSet2:
    hypoth2 = genHypo( i,'k5', 'k2', 'k15', 'k12', delta2, finalSet2, hypoth2 )

  setH2 = set(hypoth2)
  #print setH2

  #3nd Quartet
  set9 = set()
  set10 = set()
  set11 = set()
  set12 = set() 
  delta3 = {}

  ( delta3, set9 )  = fillDelta('k9', 'k9', 'k6', 'k3', 'k16', delta3, set9, 1, 8, xs, xprimes)
  ( delta3, set10 ) = fillDelta('k6', 'k9', 'k6', 'k3', 'k16', delta3, set10, 3, 5, xs, xprimes)
  ( delta3, set11 ) = fillDelta('k3', 'k9', 'k6', 'k3', 'k16', delta3, set11, 2, 2, xs, xprimes)
  ( delta3, set12 ) = fillDelta('k16', 'k9', 'k6', 'k3', 'k16', delta3, set12, 1, 15, xs, xprimes)

  finalSet3 = set9 & set10 & set11 & set12
  hypoth3 = []
  for i in finalSet3:
    hypoth3 = genHypo( i,'k9', 'k6', 'k3', 'k16', delta3, finalSet3, hypoth3 )

  setH3 = set(hypoth3)

  #4th Quartet
  set13 = set()
  set14 = set()
  set15 = set()
  set16 = set() 
  delta4 = {}

  ( delta4, set13 )  = fillDelta('k13', 'k13', 'k10', 'k7', 'k4', delta4, set13, 3, 12, xs, xprimes)
  ( delta4, set14 ) = fillDelta('k10', 'k13', 'k10', 'k7', 'k4', delta4, set14, 2, 9, xs, xprimes)
  ( delta4, set15 ) = fillDelta('k7', 'k13', 'k10', 'k7', 'k4', delta4, set15, 1, 6, xs, xprimes)
  ( delta4, set16 ) = fillDelta('k4', 'k13', 'k10', 'k7', 'k4', delta4, set16, 1, 3, xs, xprimes)
  #print delta4
  finalSet4 = set13 & set14 & set15 & set16
  #print finalSet4
  hypoth4 = []
  for i in finalSet4:
    hypoth4 = genHypo( i,'k13', 'k10', 'k7', 'k4', delta4, finalSet4, hypoth4 )

  setH4 = set(hypoth4)
  print "Returning final set of hypothesis"
  return (setH1, setH2, setH3, setH4, xs, xprimes, m2, X)
  
def calc4F(k1,k2,k3,k4,k5,k6,k7,k8,k9,k10,k11,k12,k13,k14,k15,k16,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,xp1,xp2,xp3,xp4,xp5,xp6,xp7,xp8,xp9,xp10,xp11,xp12,xp13,xp14,xp15,xp16 ,m2, X):
  #1st
  f_prime = gf_mul(rsbox[gf_mul((rsbox[x1^k1]^k1^sbox[k14^k10]^aes_round_constant[9]),14)^gf_mul(11,(rsbox[x14^k14]^(k2^sbox[k15^k11])))^gf_mul(13,(rsbox[x11^k11]^(k3^sbox[k16^k12])))^gf_mul(9,(rsbox[x8^k8]^(k4^sbox[k13^k9])))]^ \
  rsbox[gf_mul((rsbox[xp1^k1]^k1^sbox[k14^k10]^aes_round_constant[9]),14)^gf_mul(11,(rsbox[xp14^k14]^(k2^sbox[k15^k11])))^gf_mul(13,(rsbox[xp11^k11]^(k3^sbox[k16^k12])))^gf_mul(9,(rsbox[xp8^k8]^(k4^sbox[k13^k9])))], gf_inv(2))        
  #2nd
  f_prime1= rsbox[gf_mul(9,(rsbox[x13^k13]^(k13^k9)))^gf_mul(14,(rsbox[x10^k10]^(k10^k14)))^gf_mul(11,(rsbox[x7^k7]^(k15^k11)))^gf_mul(13,(rsbox[x4^k4]^(k16^k12)))]^ \
  rsbox[gf_mul(9,(rsbox[xp13^k13]^(k13^k9)))^gf_mul(14,(rsbox[xp10^k10]^(k10^k14)))^gf_mul(11,(rsbox[xp7^k7]^(k15^k11)))^gf_mul(13,(rsbox[xp4^k4]^(k16^k12)))]

  if f_prime == f_prime1 :
    
    f_prime2=rsbox[gf_mul(13,(rsbox[x9^k9]^(k9^k5)))^gf_mul(9,(rsbox[x6^k6]^(k10^k6)))^gf_mul(14,(rsbox[x3^k3]^(k11^k7)))^gf_mul(11,(rsbox[x16^k16]^(k12^k8)))]^ \
    rsbox[gf_mul(13,(rsbox[xp9^k9]^(k9^k5)))^gf_mul(9,(rsbox[xp6^k6]^(k10^k6)))^gf_mul(14,(rsbox[xp3^k3]^(k11^k7)))^gf_mul(11,(rsbox[xp16^k16]^(k12^k8)))]
    if f_prime1 == f_prime2 :
      f_prime3=gf_mul(rsbox[gf_mul(11,(rsbox[x5^k5]^(k5^k1)))^gf_mul(13,(rsbox[x2^k2]^(k6^k2)))^gf_mul(9,(rsbox[x15^k15]^(k7^k3)))^gf_mul(14,(rsbox[x12^k12]^(k8^k4)))]^ \
      rsbox[gf_mul(11,(rsbox[xp5^k5]^(k5^k1)))^gf_mul(13,(rsbox[xp2^k2]^(k6^k2)))^gf_mul(9,(rsbox[xp15^k15]^(k7^k3)))^gf_mul(14,(rsbox[xp12^k12]^(k8^k4)))],gf_inv(3))
      if f_prime2 == f_prime3:
        #print f_prime3
        key = [k1,k2,k3,k4,k5,k6,k7,k8,k9,k10,k11,k12,k13,k14,k15,k16]
        print "Starting Key Reverse: "
        res = keyReverse(key)
        if aes( res, m2) == X:
          print("success")
          print key
          exit()
        else:
          print key
          print ("fail")

def stage2(setH1, setH2, setH3, setH4, xs, xprimes, m2, X):
  print "Started Stage2"
  x1 = int(xs[0],16)
  x2 = int(xs[1],16)
  x3 = int(xs[2],16)
  x4 = int(xs[3],16)
  x5 = int(xs[4],16)
  x6 = int(xs[5],16)
  x7 = int(xs[6],16)
  x8 = int(xs[7],16)
  x9 = int(xs[8],16)
  x10 = int(xs[9],16)
  x11 = int(xs[10],16)
  x12 = int(xs[11],16)
  x13 = int(xs[12],16)
  x14 = int(xs[13],16)
  x15 = int(xs[14],16)
  x16 = int(xs[15],16)

  xp1 = int(xprimes[0],16)
  xp2 = int(xprimes[1],16)
  xp3 = int(xprimes[2],16)
  xp4 = int(xprimes[3],16)
  xp5 = int(xprimes[4],16)
  xp6 = int(xprimes[5],16)
  xp7 = int(xprimes[6],16)
  xp8 = int(xprimes[7],16)
  xp9 = int(xprimes[8],16)
  xp10 = int(xprimes[9],16)
  xp11 = int(xprimes[10],16)
  xp12 = int(xprimes[11],16)
  xp13 = int(xprimes[12],16)
  xp14 = int(xprimes[13],16)
  xp15 = int(xprimes[14],16)
  xp16 = int(xprimes[15],16)
  count = 0
  pool = Pool()

  for tuple1 in setH1:
    k1  = tuple1[0]
    k14 = tuple1[1]
    k11 = tuple1[2]
    k8  = tuple1[3]
    for tuple2 in setH2:
      k5  = tuple2[0]
      k2  = tuple2[1]
      k15 = tuple2[2]
      k12 = tuple2[3]
      for tuple3 in setH3:
        k9  = tuple3[0]
        k6  = tuple3[1]
        k3  = tuple3[2]
        k16 = tuple3[3]
        for tuple4 in setH4: 
          k13 = tuple4[0] 
          k10 = tuple4[1]
          k7  = tuple4[2]
          k4  = tuple4[3]
          pool.apply_async(calc4F,(k1,k2,k3,k4,k5,k6,k7,k8,k9,k10,k11,k12,k13,k14,k15,k16,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,xp1,xp2,xp3,xp4,xp5,xp6,xp7,xp8,xp9,xp10,xp11,xp12,xp13,xp14,xp15,xp16 ,m2, X))
  pool.close()
  pool.join()

def interact_test():
  r = '8'
  f = '1'
  p = '0'
  i = '0'
  j = '0'
  param = r+','+f+','+p+','+i+','+j

  m = gen_Random_cipher()

  #1ST FAULT
  target_in.write( "%s\n" % ("") ) ; target_in.flush()
  target_in.write( "%s\n" % ( m ) ) ; target_in.flush()
  c1 = ( target_out.readline().strip() )

  target_in.write( "%s\n" % (param) ) ; target_in.flush()
  target_in.write( "%s\n" % ( m ) ) ; target_in.flush()
  c1_prime = ( target_out.readline().strip() )

  #2ND FAULT
  target_in.write( "%s\n" % ("") ) ; target_in.flush()
  target_in.write( "%s\n" % ( m ) ) ; target_in.flush()
  c2 = ( target_out.readline().strip() )

  target_in.write( "%s\n" % (param) ) ; target_in.flush()
  target_in.write( "%s\n" % ( m ) ) ; target_in.flush()
  c2_prime = ( target_out.readline().strip() )

  return ( c1, c1_prime, c2, c2_prime)  


def attack () :
  ### FULL STAGED ATTACK ###
  ### Using stage1 and stage2 to recover the key. Runs very slow even though it's running in parallel###
  ### Comment this and uncomment the test section to see the key recovered ###

  (c, c_prime, m2, X ) = interact()
  (setH1, setH2, setH3, setH4, xs, xprimes, m2, X) = solveEquation(c, c_prime, m2, X) # return the sets of key hypothesis
  stage2(setH1, setH2, setH3, setH4, xs, xprimes, m2, X)

  ##### TEST ######
  ## UNCOMMENT THE FOLLOWING LINES TO FIND THE KEY USING 2-FAULTS ON STAGE1 ##
  ## IF THERE IS A MATCH BETWEEN THE TWO SETS OF KEY HYPOTHESYS, THAT IS THE KEY ##
  #################
  
  # (c1,c1_prime,c2,c2_prime) = interact_test() ###faulty ciphers
  # (setH1, setH2, setH3, setH4, xs, xprimes, m2, X) = solveEquation(c1,c1_prime,m2,X)
  # (setH11, setH22, setH33, setH44, xs, xprimes, m2, X) =solveEquation(c2,c2_prime,m2,X)

  # setH1Final = setH1 & setH11
  # setH2Final = setH2 & setH22
  # setH3Final = setH3 & setH33
  # setH4Final = setH4 & setH44

  # key = []
  # for item in setH1Final:
  #   for k in item:
  #     key.append(k)
  # for item in setH2Final:
  #   for k in item:
  #     key.append(k)
  # for item in setH3Final:
  #   for k in item:
  #     key.append(k)
  # for item in setH4Final:
  #   for k in item:
  #     key.append(k)
  # print "Recovered Key:"
  # print keyReverse(key)



if ( __name__ == "__main__" ) :
  # Produce a sub-process representing the attack target.
  target = subprocess.Popen( args   = sys.argv[ 1 ],
                             stdout = subprocess.PIPE, 
                             stdin  = subprocess.PIPE )

  # Construct handles to attack target standard input and output.
  target_out = target.stdout
  target_in  = target.stdin

  attack()


























          #calc4F(k1,k2,k3,k4,k5,k6,k7,k8,k9,k10,k11,k12,k13,k14,k15,k16,x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,xp1,xp2,xp3,xp4,xp5,xp6,xp7,xp8,xp9,xp10,xp11,xp12,xp13,xp14,xp15,xp16 ,m2, X)
          #1st equation
          #count = count + 1
          #print count
          #TEST
          # k1 = int('d4',16)
          # k2 = int('d7',16)
          # k3 = int('d9',16)
          # k4 = int('64',16)
          # k5= int('fc',16)
          # k6= int('21',16)
          # k7= int('4a',16)
          # k8= int('f3',16)
          # k9= int('e7',16)
          # k10= int('7a',16)
          # k11= int('b6',16)
          # k12= int('72',16)
          # k13= int('c4',16)
          # k14= int('08',16)
          # k15= int('91',16)
          # k16 = int('96',16)
          #TODO Hardcode key as the key you get from stage 1 with 2 faults
          #print k0

###### WORKING LOOP #######

          # f_prime = gf_mul(rsbox[gf_mul((rsbox[x1^k1]^k1^sbox[k14^k10]^aes_round_constant[9]),14)^gf_mul(11,(rsbox[x14^k14]^(k2^sbox[k15^k11])))^gf_mul(13,(rsbox[x11^k11]^(k3^sbox[k16^k12])))^gf_mul(9,(rsbox[x8^k8]^(k4^sbox[k13^k9])))]^ \
          # rsbox[gf_mul((rsbox[xp1^k1]^k1^sbox[k14^k10]^aes_round_constant[9]),14)^gf_mul(11,(rsbox[xp14^k14]^(k2^sbox[k15^k11])))^gf_mul(13,(rsbox[xp11^k11]^(k3^sbox[k16^k12])))^gf_mul(9,(rsbox[xp8^k8]^(k4^sbox[k13^k9])))], gf_inv(2))        
          # #2nd
          # f_prime1= rsbox[gf_mul(9,(rsbox[x13^k13]^(k13^k9)))^gf_mul(14,(rsbox[x10^k10]^(k10^k14)))^gf_mul(11,(rsbox[x7^k7]^(k15^k11)))^gf_mul(13,(rsbox[x4^k4]^(k16^k12)))]^ \
          # rsbox[gf_mul(9,(rsbox[xp13^k13]^(k13^k9)))^gf_mul(14,(rsbox[xp10^k10]^(k10^k14)))^gf_mul(11,(rsbox[xp7^k7]^(k15^k11)))^gf_mul(13,(rsbox[xp4^k4]^(k16^k12)))]

          # if f_prime == f_prime1 :
            
          #   f_prime2=rsbox[gf_mul(13,(rsbox[x9^k9]^(k9^k5)))^gf_mul(9,(rsbox[x6^k6]^(k10^k6)))^gf_mul(14,(rsbox[x3^k3]^(k11^k7)))^gf_mul(11,(rsbox[x16^k16]^(k12^k8)))]^ \
          #   rsbox[gf_mul(13,(rsbox[xp9^k9]^(k9^k5)))^gf_mul(9,(rsbox[xp6^k6]^(k10^k6)))^gf_mul(14,(rsbox[xp3^k3]^(k11^k7)))^gf_mul(11,(rsbox[xp16^k16]^(k12^k8)))]
          #   if f_prime1 == f_prime2 :
          #     f_prime3=gf_mul(rsbox[gf_mul(11,(rsbox[x5^k5]^(k5^k1)))^gf_mul(13,(rsbox[x2^k2]^(k6^k2)))^gf_mul(9,(rsbox[x15^k15]^(k7^k3)))^gf_mul(14,(rsbox[x12^k12]^(k8^k4)))]^ \
          #     rsbox[gf_mul(11,(rsbox[xp5^k5]^(k5^k1)))^gf_mul(13,(rsbox[xp2^k2]^(k6^k2)))^gf_mul(9,(rsbox[xp15^k15]^(k7^k3)))^gf_mul(14,(rsbox[xp12^k12]^(k8^k4)))],gf_inv(3))
          #     if f_prime2 == f_prime3:
          #       print f_prime3
          #       key = [k1,k2,k3,k4,k5,k6,k7,k8,k9,k10,k11,k12,k13,k14,k15,k16]
          #       print "Starting Key Reverse: "
          #       res = keyReverse(key)
          #       if aes( res, m2) == X:
          #         print("success")
          #         print K
          #         exit()
          #       else:
            #         print K
          #           print ("fail")
          #       #[54, 85, 52, 178, 47, 225, 83, 231, 174, 146, 52, 12, 73, 73, 79, 21]
          #     else:
          #       continue
          #   else:
          #     continue
          # else :
          #   continue

  ##############################